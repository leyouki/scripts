#!/bin/sh
# avec l'aide de la communauté linuxfr
# https://linuxfr.org/forums/linux-general/posts/visualiser-charge-cpu-et-memoire-mesurer-l-efficience

#set -x # exécuter et imprimer [debug]
# Création du fichier de sortie
Time=$(date)
outputfile="$Time.csv"
rm -vf "$outputfile"
# -v 	pour afficher ce qui se passe (à utiliser si tu es en mode interactif)
# -f 	au cas où tu aurais un alias avec -i
# 		ou si tu lances en tant qu'un autre compte (mais ne marchera que pour root)

# Définition du nombre de thread et/ou processeurs pour normaliser le load average
command -v nproc && NbrCrs=$(nproc --all) || NbrCrs=1 # double pipe "||": si la commande existe on l'utilise sinon on ne tient pas compte du nbr de cœurs


## Informations système
Sysinfo=$(lsb_release -a)
TotMem=$(free -t | awk '/Mem:/ {print $2}') # mémoire totale
printf '%s\t%s\t\n%s\t%s\t\n%s\t\n' "Date:" "$Time" "Mémoire totale:" "$TotMem" "$Sysinfo" >> $outputfile

## Inscription des entêtes du fichier de sortie
printf '%s\t%s\t%s\t%s\t%s\t\n' "Timestamp" "Mémoire disponible" "Charge CPU" >> $outputfile

## Enregistrement des données dans le fichier de sortie
while true;
do
# Définition des variables
TheTime=$(date +"%T")
AvMem=$(free -t | awk '/Mem:/ {print $7}') # mémoire disponible
TheLoad=$(uptime | awk -v N=$NbrCrs '{print $10/N}') # load average de la dernière
printf '%s\t%s\t%s\t%s\t\n' "$TheTime" "$AvMem"  "$TheLoad" >> $outputfile
sleep 2
done
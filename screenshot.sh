#!/bin/bash

# Crée un répertoire pour stocker les captures d'écran
mkdir -p ~/captures

# Boucle infinie pour prendre des captures d'écran toutes les 5 secondes
while true; do
    # Prend une capture d'écran et la sauvegarde avec un nom basé sur la date et l'heure actuelles
    scrot ~/captures/screenshot_$(date +%Y-%m-%d_%H-%M-%S).png
    # Attend 5 secondes avant de prendre la prochaine capture d'écran
    sleep 5
done